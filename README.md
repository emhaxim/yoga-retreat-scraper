# Yoga Retreat Scraper



## Description
#### Yoga Retreat Scraper is a django based app. It scrapes data from [https://www.bookyogaretreats.com/]. 
#### This website use cloudflare protection to detect bot. 
#### Two different approaches are used to avoid bot detection.

1. headless selenium scraper.
2. cloudscraper.



## Cloning Project

```
cd existing_repo
git remote add origin https://gitlab.com/emhaxim/yoga-retreat-scraper.git
git branch -M main
git push -uf origin main

```

## Project Directory
```
└───yoga-retreat-scraper
    │   README.md
    │   requirements.txt
    │
    └───scrapper
        │   db.sqlite3
        │   manage.py
        │
        ├───backend
        │   │   asgi.py
        │   │   settings.py
        │   │   urls.py
        │   │   wsgi.py
        │   │   __init__.py
        │   │
        │   └───__pycache__
        │           settings.cpython-311.pyc
        │           urls.cpython-311.pyc
        │           wsgi.cpython-311.pyc
        │           __init__.cpython-311.pyc
        │
        ├───utlities
        │       convert_db_to_csv.py
        │       get_initials.py
        │       listing_details.py
        │       listing_details_selenium.py
        │
        └───yoga_retreat
            │   admin.py
            │   apps.py
            │   models.py
            │   tests.py
            │   urls.py
            │   views.py
            │   __init__.py

  ```   
      


## Installing Dependencies from requirements.txt
- pip install -r requirements.txt 

## To explore Database
- You can use SQliteviewer extension in Vscode to view db.sqlite database
- Database have 2 main columns:
1. ListingInitials (To store the id, url identifier, etc)
2. ListingDetails (To store the other details of the listing)

## Understanding Flow
1. Step 01:
- Firstly run the get_initails.py file
- It will scrape and store initial data in the database (ListingInitial Table)

2. Step 02:
- Once initial data for each listing is stored, you will extract the URLS from ListingInitial Table
- Run the listing_details_selenium.py file to scrape and store data in ListingDetails Table.

##### Note: How to run both files are mentioned below.

## To Run the get_initails.py for listing initail data
- Open the get_initails.pyfile
- Path: scrapper > utilities > get_initails.py
- Run get_initails.py


## To Run the listing_details_selenium.py for listing detail data
- Open the listing_details_selenium.py file
- Path: scrapper > utilities > listing_details_selenium.py
- Run listing_details_selenium.py

## To Make Django Server Up
- It is not needed to make django server run for scraping.
- But If you want to run use the following command:
- python manage.py runserver.
