from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("yoga-retreat/", include("yoga_retreat.urls")),
    path("admin/", admin.site.urls),
]



