from django.forms.models import model_to_dict
import csv

import sys, os, django

#=====================================
## # Making backend.Settings Accesible
##====================================
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
django.setup()

## Importing ListingInitials table from our backend
from yoga_retreat.models import ListingInitials, ListingDetails
from datetime import datetime,timedelta


print("django setup done")
# Fetching all listings from db
all_listings = ListingDetails.objects.filter() #.filter(created_date__gte=(datetime.now() - timedelta(days=7)))
# Listings.objects.all().delete()
# storing each record as a dict in a list
data = [ model_to_dict(record) for record in all_listings ]
# print(data)
# Setting the column names
fieldnames = list(data[0].keys())


# Writing to CSV
with open('Yoga_Listing_Details.csv', 'w', encoding='UTF8', newline='') as f:
    writer = csv.DictWriter(f, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerows(data)



print(f'Data has been saved || filename: Yoga_Listing_Details.csv || Records Length: {len(data)}')



# file_name = "final_Property_Extended_Match.csv"
# # all_listings2 = Listings.objects.filter(neighbourhood='0')
# with open(file_name, "w", encoding='utf-8') as f:
#     f.write("current_month, current_year, property_id, Property_type, listing_title, listing_type, bedrooms, created_date, last_scraped, country, state, city, zipcode, neighbourhood, metro_statistical_area, currency, average_daily_rate_USD, average_daily_rate_native, annual_revenue_LTM_USD, annual_revenue_LTM_native, occuancy_rate_LTM, number_of_bookings_LTM, number_of_reviews, bathrooms, max_guests, calender_last_updated, response_rate, airbnb_response_time, airbnb_superhost, homeaway_premier_partner, cancellation_policy, Security_deposit_USD, Security_deposit_native, cleaning_fee_USD, cleaning_fee_native, extra_people_fee_USD, extra_people_fee_native, published_nightly_rate_USD, published_nightly_rate_native, published_weekly_rate_USD, published_monthly_rate_USD, check_in_time, check_out_time, minimum_stay, count_reservation_days_LTM, count_available_days_LTM, count_blocked_days_LTM, no_of_photos, instantbook_enabled, listing_url, listing_main_image_url, listing_images, latitude, longtitude, exact_location, overall_rating, airbnb_communication_rating, airbnb_accuracy_rating, airbnb_cleanliness_Rating, airbnb_checkin_Rating, airbnb_location_Rating, airbnb_value_Rating, pets_allowed, integrated_Property_Manager, amenities, homeAway_location_type, airbnb_property_plus, airbnb_home_collection, license, airbnb_property_id, airbnb_host_id, homeAway_property_id, homeAway_property_manager_id\n")
#     for listing in all_listings:
#         # print(type(listing.neighbourhood))
#         # print(listing.airbnb_response_time)
#         listing.calender_last_updated = str(listing.calender_last_updated).split(".")[0]
#         print(listing.response_rate,listing.calender_last_updated)
#         if listing.neighbourhood:
#             listing.neighbourhood = listing.neighbourhood.replace("object","")
#         listing.listing_images = listing.listing_images.replace(",","-")
#         listing.listing_title = listing.listing_title.replace(",","-")
#         if listing.response_rate:
#             listing.response_rate = int(listing.response_rate)
#         listing.metro_statistical_area = ""
#         if listing.airbnb_property_plus:
#             listing.airbnb_property_plus = listing.airbnb_property_plus.replace(",","-")
#         if listing.Security_deposit_USD:
#             listing.Security_deposit_USD = listing.Security_deposit_USD.replace(",","-").replace("object","")
#         if listing.Security_deposit_native:
#             listing.Security_deposit_native = listing.Security_deposit_native.replace(",","-").replace("object","")
#         if listing.currency:
#             listing.currency = listing.currency.replace(",","")
#         if listing.amenities:
#             listing.amenities = listing.amenities.replace(",","-")
#         if listing.integrated_Property_Manager:
#             listing.integrated_Property_Manager = listing.integrated_Property_Manager.replace(",","-")
#         if listing.airbnb_response_time:
#             listing.airbnb_response_time = listing.airbnb_response_time.replace(",","-")
#         if listing.neighbourhood == "0":
#             listing.response_rate = listing.airbnb_response_time
#             listing.airbnb_response_time = listing.airbnb_superhost
#             listing.airbnb_superhost = listing.homeaway_premier_partner
#             listing.homeaway_premier_partner = listing.cancellation_policy
#             listing.cancellation_policy = listing.Security_deposit_USD
#             listing.Security_deposit_USD = ""

#         f.write(f"{listing.current_month}, {listing.current_year}, {listing.property_id}, {listing.Property_type}, {listing.listing_title}, {listing.listing_type}, {listing.bedrooms}, {listing.created_date}, {listing.last_scraped}, {listing.country}, {listing.state}, {listing.city}, {listing.zipcode}, {listing.neighbourhood}, {listing.metro_statistical_area}, {listing.currency}, {listing.average_daily_rate_USD}, {listing.average_daily_rate_native}, {listing.annual_revenue_LTM_USD}, {listing.annual_revenue_LTM_native}, {listing.occuancy_rate_LTM}, {listing.number_of_bookings_LTM}, {listing.number_of_reviews}, {listing.bathrooms}, {listing.max_guests}, {listing.calender_last_updated}, {listing.response_rate}, {listing.airbnb_response_time}, {listing.airbnb_superhost}, {listing.homeaway_premier_partner}, {listing.cancellation_policy}, {listing.Security_deposit_USD}, {listing.Security_deposit_native}, {listing.cleaning_fee_USD}, {listing.cleaning_fee_native}, {listing.extra_people_fee_USD}, {listing.extra_people_fee_native}, {listing.published_nightly_rate_USD}, {listing.published_nightly_rate_native}, {listing.published_weekly_rate_USD}, {listing.published_monthly_rate_USD}, {listing.check_in_time}, {listing.check_out_time}, {listing.minimum_stay}, {listing.count_reservation_days_LTM}, {listing.count_available_days_LTM}, {listing.count_blocked_days_LTM}, {listing.no_of_photos}, {listing.instantbook_enabled}, {listing.listing_url}, {listing.listing_main_image_url}, {listing.listing_images}', {listing.latitude}, {listing.longtitude}, {listing.exact_location}, {listing.overall_rating}, {listing.airbnb_communication_rating}, {listing.airbnb_accuracy_rating}, {listing.airbnb_cleanliness_Rating}, {listing.airbnb_checkin_Rating}, {listing.airbnb_location_Rating}, {listing.airbnb_value_Rating}, {listing.pets_allowed}, {listing.integrated_Property_Manager}, {listing.amenities}, {listing.homeAway_location_type}, {listing.airbnb_property_plus}, {listing.airbnb_home_collection}, {listing.license}, {listing.airbnb_property_id}, {listing.airbnb_host_id}, {listing.homeAway_property_id}, {listing.homeAway_property_manager_id}\n")
#         # break
#         # sleep(2)