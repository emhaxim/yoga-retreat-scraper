#===================================
## # Importing Packages/Dependencies
##==================================S
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from time import sleep
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
import os, sys, django, platform, warnings, re
from bs4 import BeautifulSoup
warnings.filterwarnings("ignore")


#=====================================
## # Making backend.Settings Accesible
##====================================
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
django.setup()

## Importing ListingInitials & ListingDetails tables from our backend
from yoga_retreat.models import ListingInitials, ListingDetails


##========================
## # Getting data from db
##========================
listings = ListingInitials.objects.filter().values()


##===========================
## # Configure Chrome options
##===========================
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36")
chrome_options.add_argument('start-maximized')
# chrome_options.add_argument('--proxy-server={}:{}'.format(proxy_address, proxy_port))



index = 1
for i in range(862, 2000):  
    # try:
       
        link = listings[i]['listing_url']

        index += 1
        ##=================
        ## # Modifying URL
        ##=================
        link = "https://www.bookyogaretreats.com" + link 
        # URLs.append(url)

        if platform.system() == "Linux":
            driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
        else:
            chrome_driver_path = r"C:\Users\X1-Carbon\Downloads\chromedriver-win64\chromedriver-win64\chromedriver.exe"
            driver = webdriver.Chrome(executable_path=chrome_driver_path, options=chrome_options)
           # driver = webdriver.Chrome(executable_path="chromedriver", options=chrome_options)

        print(f'On iteration # {i}')
        print("Going to %s"%link)
        driver.get(link)
        sleep(4)
        print('Title : ',driver.title.encode('utf-8'))
        sleep(4)
        # print(driver.page_source.encode("utf-8"))
        soup = BeautifulSoup(driver.page_source, "html.parser")
        # print(soup.encode('utf-8'))

        ##===============
        ## # Listing ID
        ##===============
        listing_id = 0
        # script_tag = soup.find('script', {'data-should-stay': ''})
        # print('Script Tag : ',script_tag)
        # script_content = script_tag.string
        listing_id_match = re.search(r"'listingId': (\d+)", str(soup))
        if listing_id_match:
            listing_id = listing_id_match.group(1)


        ##=================
        ## # Listing Images
        ##=================
        images  = ''

        images_element = soup.find_all('img', class_='media-gallery-img')
        if images_element:
            images = [img.get("src") for img in images_element]

        
        ##===============
        ## # Title
        ##===============
        title = ''
        title_element = soup.find('h1', class_='listing-title__title title listing-title-new')
        if title_element:
            title = title_element.text
       

        ##===============
        ## # Subtitle
        ##===============
        subtitle = ''
        subtitle_element = soup.find('div', class_='listing-title__location listing-location-new')
        if subtitle_element:
            subtitle = subtitle_element.text
       

        ##============
        ## # Overview
        ##============
        # Find all anchor elements with class='js-showcard-link'
        overview = soup.find('div', class_='listing-overview__introduction')
        # overview_list.append(overview.text)
        # print(overview.text)

        ##===============
        ## # Organized by
        ##===============
        organizer = soup.find('div', class_='listing-organizer-card__content')
        
        # organizer_list.append(organizer.text)
        # print(organizer.text)


        ##==================
        ## # Yoga highlights
        ##==================
        highlights = ''
        highlights_element = soup.find('div', class_='listing-description-container')
        if highlights_element:
          highlights = str(highlights_element.find_all('ul')[-1])
        

        ##=======================
        ## # Check In & Check Out
        ##=======================
        time = soup.find_all('div', class_='checkin-out--time')
        check_in_time = ''
        check_out_time = ''
        if len(time) > 1:
            check_in_time = time[0]
            check_out_time = time[-1]


        ##===============
        ## # Skill level
        ##===============
        skill_level = soup.find('div', class_='listing-description-container')
        if skill_level:
            if(len(skill_level.find_all('ul')) > 2):
                skill_level = skill_level.find_all('ul')[1]
            else:
                skill_level = ""


        ##===============
        ## # Yoga Styles
        ##===============
        styles = soup.find('div', class_='listing-description-container')
        # styles_list.append(str(styles.find('ul')))
        # print(styles.find('ul'))


        ##===============
        ## # Instructors
        ##===============
        instructors = soup.find_all('div', class_='card card-instructor')

        for i in range(len(instructors)):
            try:
                # finding tag whose child to be deleted
                certificate_text = instructors[i].find('div', class_='certificate-text')
                # delete the child element
                certificate_text.decompose()
            except Exception as e:
                pass
                # print('Certificate Text Not Found!')

        instructors = [instructor.text.replace("Show","").replace("Hide","") for instructor in instructors]
        # instructors_list.append(instructors)
        # print(instructors)  
        
        ##===============
        ## # Location
        ##===============
        location = ''
        location_element = soup.find('div', class_='property-container')
        if location_element:
            location = location_element.text


        ##===============
        ## # Excursion
        ##===============
        excursion = soup.select_one('div[data-attr="lp-excursions"]')


        ##===============
        ## # Program
        ##===============
        program  = soup.select_one('div[data-attr="lp-itinerary"]') 


        ##===================
        ## # Food Description
        ##===================
        try:
            food = soup.select_one('div[data-attr="lp-food"]')
            food_description = food.find_all('p')
            food_description = [p.text for p in food_description]
            food_description = " ".join(food_description)

        except:
             print(f'Food doesnot exist in {link}')
             food_description = ""


        ##===========================================================
        ## # Types of Meal / Drinks / dietary requirement(s) Included
        ##===========================================================
        food_type = soup.find_all('div', class_='structured_content')
        if food_type:
            food_type = [d.text for d in food_type]
        
        # print(f'Food type : {food_type}')
        meal = ''
        drinks = ''
        dietary = ''
        if len(food_type) >=1 :
            meal = food_type[0]
        if len(food_type) > 1:
            for i in range(1, len(food_type)):
                    if 'drinks' in food_type[i]:
                       drinks = food_type[i]
                    if 'dietary' in food_type[i]:
                        dietary = food_type[i]

                  
        ##=====================
        ## # Health and Hygiene
        ##=====================
        health_and_hyg = ''
        health_and_hyg_element = soup.find('div', class_='health-and-hygiene-detail')
        if health_and_hyg_element:
            health_and_hyg = health_and_hyg_element.text

        ##===============
        ## # Accomodation
        ##===============
        accomodation = ''
        accomodation_element = soup.select_one('div[data-attr="lp-accommodation"]')
        if accomodation_element:
            accomodation = accomodation_element.text

        ##======================
        ## # Arrival Information
        ##======================
        arrival_info = ''
        arrival_info_element = soup.find('div', id='how_to_get_there')
        if arrival_info_element:
            arrival_info = arrival_info_element.text

        ##============
        ## # Included
        ##============
        included = ''
        included_element = soup.select_one('div[data-attr="lp-whats-included"]')    
        if included_element:
                included = included_element


        ##============
        ## # Excluded
        ##============
        excluded = ''
        excluded_element = soup.select_one('div[data-attr="lp-whats-not-included"]')    
        if excluded_element:
                excluded = excluded_element

        ##======================
        ## # Cancellation Policy
        ##======================
        cancellation_policy = ''
        cancellation_policy_element = soup.find_all('div', class_='section collapsible full-width-sm')
        if cancellation_policy_element:
            if 'Cancellation Policy' in cancellation_policy_element[-1].text:
                cancellation_policy = cancellation_policy_element[-1]


        ##================
        ## # Spa Treatment
        ##================
        treatment = ''
        treatment_element = soup.select_one('div[data-attr="lp-spa-treatments"]')    
        if treatment_element:
                treatment = treatment_element


  
       
        sleep(1)

        
        # exit()
        ##=============================================
        ## # Number of records in Listing_Details Table
        ##=============================================
        print('Record in Listing_Details Table : ',len(ListingDetails.objects.filter()),'\n\n')
        
        ##==========================================
        ## # Creating a record Listing_Details Table
        ##==========================================
        listing_details = ListingDetails.objects.create(
        listing_id=listing_id,
        listing_images = images,
        listing_url= link,
        listing_title=title,
        listing_subtitle=str(subtitle).strip(),
        listing_overview=str(overview).strip(),
        listing_highlights=str(highlights).strip(),
        listing_yoga_style=str(styles).strip(),
        listing_yoga_skill_level=str(skill_level).strip(),
        listing_organizer=str(organizer).strip(),
        listing_location=str(location).strip(),
        listing_program=str(program).strip(),
        listing_excursion=str(excursion).strip(),
        listing_instructors=str(instructors).strip(),
        listing_check_in = str(check_in_time).strip(),  
        listing_check_out = str(check_out_time).strip(),
        listing_food_description = str(food_description).strip() ,
        listing_meal = str(meal).strip(),
        listing_drinks = str(drinks).strip(),
        listing_dietry = str(dietary).strip() ,
        listing_health_and_hyg = str(health_and_hyg).strip(),
        listing_accomodation = str(accomodation).strip(),    
        listing_arrival_info = str(arrival_info).strip(),
        listing_cancellation_policy = str(cancellation_policy).strip(),
        listing_included = str(included).strip(),
        listing_excluded = str(excluded).strip(),
        listing_treatment = str(treatment).strip(),

         ).save()

        























#
# cities_to_search = [
#     "Destin",
#     "Santa Rosa Beach",
#     "Miramar Beach",
#     "Fort Walton Beach"
# ]
# ​
# cities_to_search_new = [
#     "Destin",
#     "SantaRosaBeach",
#     "MiramarBeach",
#     "FortWaltonBeach",
#     "SantaRosaBeach",
#     "SantaRosaBeach,Florida"
# ]
# ​
# proxy_address = '69.30.227.194'
# proxy_port = 2000
# ​
# # Configure Chrome options
# chrome_options = Options()
# chrome_options.add_argument("--headless")
# chrome_options.add_argument('--no-sandbox')
# chrome_options.add_argument('--disable-dev-shm-usage')
# chrome_options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36")
# ​
# # chrome_options.add_argument('--proxy-server={}:{}'.format(proxy_address, proxy_port))
# chrome_options.add_argument('start-maximized')
# ​
# link = "https://www.bookyogaretreats.com/firefly-retreat/7-day-wellness-yoga-retreat-in-ubud-bali"
# ​
# if platform.system() == "Linux":
#     driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
# else:
#     driver = webdriver.Chrome(executable_path="chromedriver", options=chrome_options)
# ​
# driver.maximize_window()
# sleep(1)
# print("Going to %s"%link)
# driver.get(link)
# sleep(4)
# print(driver.title)
# sleep(10)
# print(driver.page_source)
# driver.quit()
