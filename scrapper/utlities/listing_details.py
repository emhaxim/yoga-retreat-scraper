##======================
## # Including Packages
##=====================
import cloudscraper
from bs4 import BeautifulSoup
from time import sleep
import pandas as pd
import json
import sys, os, django, re


#===================
## # Setting Proxies
##==================
# proxies = {
#     "http":"http://69.30.227.194:2000",
#     "https":"https://69.30.227.194:2000"
# }

proxies = {
    "http":"http://186.121.235.66:8080",
    "https":"http://186.121.235.66:8080"
}


# pip install zenrows
# from zenrows import ZenRowsClient

# client = ZenRowsClient("8332d2188b0c0c89b6796fba937cdeed9f03563f")
# url = "https://www.bookyogaretreats.com/voice-of-plenty/14-day-indus-valley-winter-tour-and-yoga-retreat-in-pakistan"

# response = client.get(url)

# print(response.text)

#=====================================
## # Making backend.Settings Accesible
##====================================
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
django.setup()

## Importing ListingInitials & ListingDetails tables from our backend
from yoga_retreat.models import ListingInitials, ListingDetails


##=============================================
## # Getting data from db
##=============================================  
listings = ListingInitials.objects.filter().values()
# print(listings[10]['listing_url'])


##=============================== 
## # Initializing multiple lists
##===============================
overview_list = []
organizer_list = []
highlights_list = []
styles_list = []
instructors_list = []
food_list = []
URLs = []


##=============================== 
## # Iterating over listings Id
##===============================
index = 1
for i in range(2778):  
    # try:
       
        url = listings[i]['listing_url']

        index += 1
        ##=================
        ## # Modifying URL
        ##=================
        url = "https://www.bookyogaretreats.com" + url 
        # URLs.append(url)

        print(f'On iteration # {i} : URL : {url}')
        
                                                                                                                                                                                                                                                                                                                                                              
        scraper = cloudscraper.create_scraper(   browser={
        'browser': 'chrome',
        'platform': 'android',
        'desktop': False
    })
        response = scraper.get(url)  #   ,  proxies=proxies)
        print('Status Code : ',response.status_code)

        count = 2
        while response.status_code != 200 :
            scraper = cloudscraper.create_scraper()
            response = scraper.get(url) #, proxies=proxies)
            print(f'Status Code for {count} try : ',response.status_code)
            count += 1
            sleep(5)
            if count > 5:
                 break



        # Making soup from response
        soup = BeautifulSoup(response.text, "html.parser")
        # print(soup.encode("utf-8"))

        ##===============
        ## # Listing ID
        ##===============
        listing_id = 0
        script_tag = soup.find('script', {'data-should-stay': ''})
        script_content = script_tag.string
        listing_id_match = re.search(r"'listingId': (\d+)", script_content)
        if listing_id_match:
            listing_id = listing_id_match.group(1)


        ##=================
        ## # Listing Images
        ##=================
        images  = ''

        images_element = soup.find_all('img', class_='media-gallery-img')
        if images_element:
            images = [img.get("src") for img in images_element]

        
        ##===============
        ## # Title
        ##===============
        title = ''
        title_element = soup.find('h1', class_='listing-title__title title listing-title-new')
        if title_element:
            title = title_element.text
       

        ##===============
        ## # Subtitle
        ##===============
        subtitle = ''
        subtitle_element = soup.find('div', class_='listing-title__location listing-location-new')
        if subtitle_element:
            subtitle = subtitle_element.text
       

        ##============
        ## # Overview
        ##============
        # Find all anchor elements with class='js-showcard-link'
        overview = soup.find('div', class_='listing-overview__introduction')
        # overview_list.append(overview.text)
        # print(overview.text)

        ##===============
        ## # Organized by
        ##===============
        organizer = soup.find('div', class_='listing-organizer-card__content')
        
        # organizer_list.append(organizer.text)
        # print(organizer.text)


        ##==================
        ## # Yoga highlights
        ##==================
        highlights = ''
        highlights_element = soup.find('div', class_='listing-description-container')
        if highlights_element:
          highlights = str(highlights_element.find_all('ul')[-1])
        

        ##=======================
        ## # Check In & Check Out
        ##=======================
        time = soup.find_all('div', class_='checkin-out--time')
        check_in_time = ''
        check_out_time = ''
        if len(time) > 1:
            check_in_time = time[0]
            check_out_time = time[-1]


        ##===============
        ## # Skill level
        ##===============
        skill_level = soup.find('div', class_='listing-description-container')
        if skill_level:
            if(len(skill_level.find_all('ul')) > 2):
                skill_level = skill_level.find_all('ul')[1]
            else:
                skill_level = ""


        ##===============
        ## # Yoga Styles
        ##===============
        styles = soup.find('div', class_='listing-description-container')
        # styles_list.append(str(styles.find('ul')))
        # print(styles.find('ul'))


        ##===============
        ## # Instructors
        ##===============
        instructors = soup.find_all('div', class_='card card-instructor')

        for i in range(len(instructors)):
            try:
                # finding tag whose child to be deleted
                certificate_text = instructors[i].find('div', class_='certificate-text')
                # delete the child element
                certificate_text.decompose()
            except Exception as e:
                pass
                # print('Certificate Text Not Found!')

        instructors = [instructor.text.replace("Show","").replace("Hide","") for instructor in instructors]
        # instructors_list.append(instructors)
        # print(instructors)  
        
        ##===============
        ## # Location
        ##===============
        location = ''
        location_element = soup.find('div', class_='property-container')
        if location_element:
            location = location_element.text


        ##===============
        ## # Excursion
        ##===============
        excursion = soup.select_one('div[data-attr="lp-excursions"]')


        ##===============
        ## # Program
        ##===============
        program  = soup.select_one('div[data-attr="lp-itinerary"]') 


        ##===================
        ## # Food Description
        ##===================
        try:
            food = soup.select_one('div[data-attr="lp-food"]')
            food_description = food.find_all('p')
            food_description = [p.text for p in food_description]
            food_description = " ".join(food_description)

        except:
             print(f'Food doesnot exist in {url}')
             food_description = ""


        ##===========================================================
        ## # Types of Meal / Drinks / dietary requirement(s) Included
        ##===========================================================
        food_type = soup.find_all('div', class_='structured_content')
        if food_type:
            food_type = [d.text for d in food_type]
        
        # print(f'Food type : {food_type}')
        meal = ''
        drinks = ''
        dietary = ''
        if len(food_type) >=1 :
            meal = food_type[0]
        if len(food_type) > 1:
            for i in range(1, len(food_type)):
                    if 'drinks' in food_type[i]:
                       drinks = food_type[i]
                    if 'dietary' in food_type[i]:
                        dietary = food_type[i]

                  
        ##=====================
        ## # Health and Hygiene
        ##=====================
        health_and_hyg = ''
        health_and_hyg_element = soup.find('div', class_='health-and-hygiene-detail')
        if health_and_hyg_element:
            health_and_hyg = health_and_hyg_element.text

        ##===============
        ## # Accomodation
        ##===============
        accomodation = ''
        accomodation_element = soup.select_one('div[data-attr="lp-accommodation"]')
        if accomodation_element:
            accomodation = accomodation_element.text

        ##======================
        ## # Arrival Information
        ##======================
        arrival_info = ''
        arrival_info_element = soup.find('div', id='how_to_get_there')
        if arrival_info_element:
            arrival_info = arrival_info_element.text

        ##============
        ## # Included
        ##============
        included = ''
        included_element = soup.select_one('div[data-attr="lp-whats-included"]')    
        if included_element:
                included = included_element


        ##============
        ## # Excluded
        ##============
        excluded = ''
        excluded_element = soup.select_one('div[data-attr="lp-whats-not-included"]')    
        if excluded_element:
                excluded = excluded_element

        ##======================
        ## # Cancellation Policy
        ##======================
        cancellation_policy = ''
        cancellation_policy_element = soup.find_all('div', class_='section collapsible full-width-sm')
        if cancellation_policy_element:
            if 'Cancellation Policy' in cancellation_policy_element[-1].text:
                cancellation_policy = cancellation_policy_element[-1]


        ##================
        ## # Spa Treatment
        ##================
        treatment = ''
        treatment_element = soup.select_one('div[data-attr="lp-spa-treatments"]')    
        if treatment_element:
                treatment = treatment_element


  
       
        sleep(4)

        
        # exit()
        ##=============================================
        ## # Number of records in Listing_Details Table
        ##=============================================
        print('Record in Listing_Details Table : ',len(ListingDetails.objects.filter()),'\n\n')
        
        ##==========================================
        ## # Creating a record Listing_Details Table
        ##==========================================
        # listing_details = ListingDetails.objects.create(
        # listing_id=listing_id,
        # listing_images = images,
        # listing_url= url,
        # listing_title=title,
        # listing_subtitle=str(subtitle).strip(),
        # listing_overview=str(overview).strip(),
        # listing_highlights=str(highlights).strip(),
        # listing_yoga_style=str(styles).strip(),
        # listing_yoga_skill_level=str(skill_level).strip(),
        # listing_organizer=str(organizer).strip(),
        # listing_location=str(location).strip(),
        # listing_program=str(program).strip(),
        # listing_excursion=str(excursion).strip(),
        # listing_instructors=str(instructors).strip(),
        # listing_check_in = str(check_in_time).strip(),  
        # listing_check_out = str(check_out_time).strip(),
        # listing_food_description = str(food_description).strip() ,
        # listing_meal = str(meal).strip(),
        # listing_drinks = str(drinks).strip(),
        # listing_dietry = str(dietary).strip() ,
        # listing_health_and_hyg = str(health_and_hyg).strip(),
        # listing_accomodation = str(accomodation).strip(),    
        # listing_arrival_info = str(arrival_info).strip(),
        # listing_cancellation_policy = str(cancellation_policy).strip(),
        # listing_included = str(included).strip(),
        # listing_excluded = str(excluded).strip(),
        # listing_treatment = str(treatment).strip(),

        #  ).save()

        




                                   

        # check = ListingDetails.objects.create(
        #                                 listing_id=listings[i]['listing_id'],
                                    
        #                                 listing_url=listings[i]['listing_url'],
        #                                 listing_title = 'ABC',
        #                                 listing_subtitle = subtitle,
        #                                 listing_overview = str(overview),
        #                                 listing_highlights =str( highlights),

        #                                 listing_yoga_style = str(styles),
        #                                 listing_yoga_skill_level = str(skill_level),
        #                                 listing_organizer = str(organizer),

        #                                 listing_location = str(location),
        #                                 listing_program = str(program),
        #                                 listing_excursion = str(excursion),
        #                                 listing_instructors = instructors,
                                        
        #                                 listing_food = food
                                        
        #                                )
        # print('Check :', check)
    # .save()

    # except Exception as e:
        # print(e)

        # fetched_data = {
        #     'Organizer' : organizer_list,
        #     'Overview' : overview_list,
        #     'Yoga_Styles' : styles_list,
        #     'Instructors' : instructors_list,
        #     'Food' : food_list,
        #     'Highlights' : highlights_list,
        # }
        # with open('updated_data.json', 'w', encoding="utf-8") as convert_file:
        #     convert_file.write(json.dumps(fetched_data))

        # result_df = pd.DataFrame(fetched_data)
        # result_df.to_csv('updated_data.csv')



# fetched_data = {
#             'URL':URLs,
#             'Organizer' : organizer_list,
#             'Yoga_Styles' : styles_list,
#             'Instructors' : instructors_list,
#             'Food' : food_list,
#             'Highlists' : highlights_list,
#         }


# print(fetched_data)

# with open('updated_data.json', 'w' , encoding="utf-8") as convert_file:
#             convert_file.write(json.dumps(fetched_data))

# result_df = pd.DataFrame(fetched_data)
# result_df.to_csv('updated_data.csv')


