#======================
## # Importing Packages
##=====================

import cloudscraper
from bs4 import BeautifulSoup
from time import sleep
import pandas as pd
import json
import os
import django
import sys


#=====================================
## # Making backend.Settings Accesible
##====================================
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
django.setup()

## Importing ListingInitials table from our backend
from yoga_retreat.models import ListingInitials


#===================
## # Setting Proxies
##==================
proxies = {
    "http":"http://69.30.227.194:2000",
    "https":"https://69.30.227.194:2000"
}


##=======================================================
## # Initializing page_limit and all_pages_listings list
##=======================================================
pages_limit = 500
all_pages_listing_url = []
all_pages_listing_ids = []
all_pages_listing_countries = []
all_pages_listing_people_interested = []
all_pages_listing_ratings = []
all_pages_listing_titles = []
all_pages_listing_amenities = []
all_pages_listing_max_days = []
all_pages_listing_persons = []
all_pages_listing_min_price = []


##===============
## # Defining URL
##===============
url = "https://www.bookyogaretreats.com/"


##========================================
## # Iterating Upto pre-defined page_limit
##========================================
for i in range(149,pages_limit):
    try:
        scraper = cloudscraper.create_scraper()
        
        url = url + f'?page={i}'
        print('URL inside for loop',url)
        response = scraper.get(url) #, proxies=proxies)

        # Making soup from response
        soup = BeautifulSoup(response.text, "html.parser")
        # print(soup.encode("utf-8"))

        ##================
        ## # Listing URLs
        ##================
        # Find all anchor elements with class='js-showcard-link'
        links = soup.find_all('a', class_='js-showcard-link')
        # Extract the href attribute from each anchor element
        listing_on_one_page = [link['href'] for link in links]
        all_pages_listing_url.extend(listing_on_one_page)


        ##================
        ## # Listing Ids
        ##================
        ids = soup.find_all('div', class_='showcard-listing')
        listing_ids_on_one_page = [id['data-listing-id'] for id in ids]
        all_pages_listing_ids.extend(listing_ids_on_one_page)

        ##====================
        ## # Listing Countries
        ##====================
        countries = soup.find_all('div', class_='pre-main-content__location')
        listing_countries_on_one_page = [country.text.strip() for country in countries]
        all_pages_listing_countries.extend(listing_countries_on_one_page)

        ##============================
        ## # Listing People Interested
        ##============================
        people_interested = soup.find_all('div', class_='pre-main-content__people-interested')
        listing_people_interested_on_one_page = [people_count.text.strip() for people_count in people_interested]
        all_pages_listing_people_interested.extend(listing_people_interested_on_one_page)

        print(all_pages_listing_people_interested)

        ##===================
        ## # Listing Ratings
        ##===================
        ratings = soup.find_all('div', class_='pre-main-content__rating')
        listing_ratings_on_one_page = [rating.text.strip() for rating in ratings]
        all_pages_listing_ratings.extend(listing_ratings_on_one_page)

        # print(all_pages_listing_ratings)

        ##============================
        ## # Listing Titles
        ##============================
        titles = soup.find_all('div', class_='listing-title')
        listing_titles_on_one_page = [title.text.strip() for title in titles]
        all_pages_listing_titles.extend(listing_titles_on_one_page)

        ##===============================
        ## # Listing Amenities / Features
        ##===============================
        amenities = soup.find_all('div', class_='listing-features')
        listing_amenities_on_one_page = [amenity.text.strip().replace('\n',', ') for amenity in amenities]
        all_pages_listing_amenities.extend(listing_amenities_on_one_page)

        # print(all_pages_listing_amenities)

        ##============================
        ## # Listing Max Days
        ##============================
        max_days = soup.find_all('span', class_='days')
        listing_days_on_one_page = [days.text.strip().replace('\n',', ') for days in max_days]
        all_pages_listing_max_days.extend(listing_days_on_one_page)

        # print(all_pages_listing_max_days)

        ##==================
        ## # Listing Persons
        ##==================
        max_persons = soup.find_all('span', class_='detail-item detail-item--number-of-people')
        listing_max_persons_on_one_page = [persons.text.strip().replace('\n',', ') for persons in max_persons]
        all_pages_listing_persons.extend(listing_max_persons_on_one_page)

        # print(all_pages_listing_persons)

        ##============================
        ## # Listing Max Days
        ##============================
        min_prices = soup.find_all('div', class_='price')
        listing_min_price_on_one_page = [price.text.strip().replace('\n',', ') for price in min_prices]
        all_pages_listing_min_price.extend(listing_min_price_on_one_page)

        # print(all_pages_listing_min_price)

    
        print(f'On iteration # {i} : length of all_pages_listing_url : {len(all_pages_listing_url)}')
        print(f'On iteration # {i} : length of all_pages_listing_ids  : {len(all_pages_listing_ids)}\n')
        # print(all_pages_listing_url)

        ## Iterate  over listing_ids
        for index,listing_id in enumerate(listing_ids_on_one_page):
            ## Check if listings exist in the database
          
            if len(ListingInitials.objects.filter( listing_url = listing_on_one_page[index])) == 0:
               # print(len(ListingInitials.objects.filter(listing_id=int(listing_id), listing_url = all_pages_listing_url[index])) )
                print('URL Identifier : ',listing_on_one_page[index])
                ## If not exist, add listing to db
                print("Adding new listing id: %s"%listing_id,"\n")
                ListingInitials.objects.create( listing_id=listing_id,
                                                new=True, 
                                                listing_url=listing_on_one_page[index],
                                                listing_title=listing_titles_on_one_page[index],
                                                listing_country=listing_countries_on_one_page[index],
                                                listing_features=listing_amenities_on_one_page[index],
                                                listing_event_days=listing_days_on_one_page[index],
                                                listing_person=listing_max_persons_on_one_page[index],
                                                listing_event_amount=listing_min_price_on_one_page[index]
                                               ).save()
            
            else:
                print(f"Listing Id : {listing_id} already exists")
        # print(listing_on_one_page)
        url = "https://www.bookyogaretreats.com/"
        sleep(4)
    except:
        pass

        # result = {
        #     'Identifier' : all_pages_listing_url,
        #     'Listing_ID' : all_pages_listing_ids,
        #     'Title' : all_pages_listing_titles,
        #     'Country' : all_pages_listing_countries,
        #     'People_Interested' : all_pages_listing_people_interested,
        #     'Rating' : all_pages_listing_ratings,
        #     'Features' : all_pages_listing_amenities,
        #     'Event Days' : all_pages_listing_max_days,
        #     'Person' : all_pages_listing_persons,
        #     'Amount' : all_pages_listing_min_price,
        # }

        # with open('convert.txt', 'w',  encoding="utf-8") as convert_file:
        #     convert_file.write(json.dumps(result))


        # result_df = pd.DataFrame(result)
        # result_df.to_csv('yoga_retreat.csv')

        



# result = {
#             'Identifier' : all_pages_listing_url,
#             'Listing_ID' : all_pages_listing_ids,
#             'Title' : all_pages_listing_titles,
#             'Country' : all_pages_listing_countries,
#             # 'People_Interested' : all_pages_listing_people_interested,
#             # 'Rating' : all_pages_listing_ratings,
#             'Features' : all_pages_listing_amenities,
#             'Days' : all_pages_listing_max_days,
#             'Person' : all_pages_listing_persons,
#             'Amount' : all_pages_listing_min_price,
#         }


# for i in result.values():
#     print(len(i))


# with open('convert.json', 'w') as convert_file:
#     convert_file.write(json.dumps(result))

# result_df = pd.DataFrame(result)
# result_df.to_csv('yoga_retreat.csv')


# f = open('ypga_check.txt')
# f.write
# print(str(soup))

# import warnings
# from selenium import webdriver
# # from selenium.webdriver.common.keys import Keys
# # from selenium.webdriver.common.by import By
# from selenium.webdriver.chrome.options import Options
# from time import sleep
# # from datetime import datetime
# warnings.filterwarnings("ignore")


# from datetime import date
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.wait import WebDriverWait
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.proxy import Proxy, ProxyType
# from selenium.webdriver.common.keys import Keys         

# from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.chrome.service import Service
# import platform
# from webdriver_manager.chrome import ChromeDriverManager


# #===================
# ## # Setting Proxies
# ##==================
# proxy_address = '69.30.227.194'
# proxy_port = 2000


# ##============================
# ## # Configure Chrome options
# ##============================
# chrome_options = Options()


# # chrome_options.add_argument("--headless")
# chrome_options.add_argument('--no-sandbox')
# chrome_options.add_argument('--disable-dev-shm-usage')
# chrome_options.add_argument('start-maximized')
# chrome_options.add_argument('--proxy-server={}:{}'.format(proxy_address, proxy_port))
# chrome_options.add_argument('user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36')



# ##============================
# ## # setting URL
# ##============================
# url =  "https://www.bookyogaretreats.com/"


# ##=============================
# ## # setting chrome driver
# ##=============================
# chrome_driver_path = r"C:\Users\X1-Carbon\Downloads\chromedriver-win64\chromedriver-win64\chromedriver.exe"
# driver = webdriver.Chrome(executable_path=chrome_driver_path, options=chrome_options)

# # Changing the property of the navigator value for webdriver to undefined 
# # driver.execute_script("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})") 

# ##=======================================================
# ## # Initializing page_limit and all_pages_listing_url list
# ##=======================================================
# pages_limit = 3
# all_pages_listings = []

# driver.get(url)

# ##=======================================================
# ## # Iterating Upto pre-defined page_limit
# ##=======================================================
# for i in range(pages_limit):
#     listing_on_one_page =[]

   
#     print("fetching listing for page no ",i+1,"url : ")
#     # getting listing URLs
    # listing_on_one_page = driver.find_elements('xpath', "//a[@class='js-showcard-link']")
    # listing_on_one_page = [link.get_attribute('href') for link in listing_on_one_page]
    
#     all_pages_listings.extend(listing_on_one_page)
#     print(listing_on_one_page)

#     print(len(listing_on_one_page))
    
#     sleep(3)
#     driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
#     sleep(10)
    

#     next_page = driver.find_elements('xpath', '//a[@class="close"]')[-1]
#     # sleep(5)
#     if next_page.is_enabled():
#         driver.execute_script("arguments[0].click();", next_page)
#     else:
#         print("Next Page not enabled")

# sleep(120)
# print("Total length of all links : ",len(all_pages_listings))

