from django.contrib import admin
from .models import ListingDetails, ListingInitials

admin.site.register(ListingInitials)
admin.site.register(ListingDetails)


# Register your models here.
