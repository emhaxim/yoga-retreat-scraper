# Generated by Django 4.2.3 on 2023-08-30 08:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('yoga_retreat', '0007_listingdetails_listing_cancellation_policy'),
    ]

    operations = [
        migrations.AddField(
            model_name='listingdetails',
            name='listing_excluded',
            field=models.CharField(default='', max_length=1000),
        ),
        migrations.AddField(
            model_name='listingdetails',
            name='listing_included',
            field=models.CharField(default='', max_length=1000),
        ),
    ]
