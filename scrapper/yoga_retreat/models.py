from django.db import models
from django.utils import timezone
import django
import datetime
from datetime import date




class ListingInitials(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    new = models.BooleanField(default=True)
    listing_id = models.BigIntegerField(default=0)
    listing_url = models.URLField(default="", max_length = 200)
    listing_title = models.CharField(default="", max_length=400)
    listing_country = models.CharField(default="", max_length=100)
    # listing_people_interested = models.CharField(default="", max_length=200)
    # listing_rating = models.CharField(default="", max_length=100)
    listing_features = models.CharField(default="", max_length=1000)
    listing_event_days = models.CharField(default="", max_length=200)
    listing_person = models.CharField(default="", max_length=100)
    listing_event_amount = models.CharField(default="", max_length=200)


class ListingDetails(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    # new = models.BooleanField(default=True)
    listing_id = models.BigIntegerField(default=0)
    listing_images = models.CharField(default="", max_length=1200)
    listing_url = models.URLField(default="", max_length = 200)
    listing_title = models.CharField(default="", max_length=500)
    listing_subtitle = models.CharField(default="", max_length=500)
    listing_overview = models.CharField(default="", max_length=1500)
    listing_highlights = models.CharField(default="", max_length=2000)
    
    listing_yoga_style = models.CharField(default="", max_length=500)
    listing_yoga_skill_level = models.CharField(default="", max_length=500)
    listing_organizer = models.CharField(default="", max_length=500)
    listing_location = models.CharField(default="", max_length=1500)
    listing_program = models.CharField(default="", max_length=10000)
    listing_excursion = models.CharField(default="", max_length=3000)
    listing_instructors = models.CharField(default="", max_length=3000)
    listing_food_description = models.CharField(default="", max_length=6000)

    listing_meal = models.CharField(default="", max_length=3000)
    listing_drinks = models.CharField(default="", max_length=3000)
    listing_dietry = models.CharField(default="", max_length=6000)
    listing_health_and_hyg = models.CharField(default="", max_length=3000)
    listing_accomodation = models.CharField(default="", max_length=6000)    
    listing_arrival_info = models.CharField(default="", max_length=6000)
    
    listing_check_in = models.CharField(default="", max_length=200)    
    listing_check_out = models.CharField(default="", max_length=200)
    listing_cancellation_policy = models.CharField(default="", max_length=800)

    listing_included = models.CharField(default="", max_length=1000)
    listing_excluded = models.CharField(default="", max_length=1000)
    listing_treatment = models.CharField(default="", max_length=1000)










