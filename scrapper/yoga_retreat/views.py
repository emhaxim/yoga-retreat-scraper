from django.shortcuts import render
from django.http import HttpResponse



import pandas as pd
import json
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
import string
from .models import ListingDetails
# from rest_framework.decorators import api_view
from django.core.serializers import serialize
from django.forms.models import model_to_dict
from django.core.paginator import Paginator
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.forms.models import model_to_dict
from json2html import *

# import mimetypes


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def count_of_listing(request,all="no"):
    all_listing = ListingDetails.objects.all().order_by("-created_date")    #  filter(current_month=8,price__gt=0).values_list("id").distinct()
    # latest_listings = ListingDetails.objects.filter().order_by("-last_scraped")
    # if all == "no":
    latest_listings = all_listing[:100]
    processed = []
    for latest_listing in latest_listings:
        latest_listing.created_date = str(latest_listing.created_date)
        latest_listing.listing_id = str(latest_listing.listing_id) #.split(".")[0]
        latest_listing.listing_url = str(latest_listing.listing_url)
        latest_listing.listing_title = str(latest_listing.listing_title)
        latest_listing.listing_location = str(latest_listing.listing_location)
        latest_listing.listing_accomodation = str(latest_listing.listing_accomodation)
        latest_listing.listing_subtitle = str(latest_listing.listing_subtitle)
        latest_listing.listing_images = str(eval(latest_listing.listing_images)[0])
      
    
        # if latest_listing.listing_id in processed:
        #     latest_listings.objects.filter(id=latest_listing.id).delete()
        processed.append(latest_listing.listing_id)
    context ={
        "no_of_records" : len(all_listing),
        "all":all,
        "latest_listings":latest_listings
    }
    
    return render(request, "listing_details_info.html", context)