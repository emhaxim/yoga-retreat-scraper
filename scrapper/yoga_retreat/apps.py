from django.apps import AppConfig


class YogaRetreatConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'yoga_retreat'
