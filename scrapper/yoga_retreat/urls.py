from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("count_of_listing/<str:all>", views.count_of_listing, name="count_of_listing"),
]


